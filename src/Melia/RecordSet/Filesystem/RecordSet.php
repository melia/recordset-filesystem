<?php

namespace Melia\RecordSet\Filesystem;

use Melia\RecordSet\Common\RecordSet\RecordSet as RecordSetInterface;
use Melia\RecordSet\Common\Converter\OffsetConverter;
use Melia\RecordSet\Reference\Converter\NamespaceOffsetConverter;
use Melia\Uuid\Reference\Validator\Validator;
use Melia\RecordSet\Filesystem\Exception\MetadataNotFoundException;
use Melia\RecordSet\Filesystem\Exception\UnsupportedMetadataException;
use Melia\RecordSet\Filesystem\Exception\MissingMetadataException;
use Melia\RecordSet\Filesystem\Exception\UnsupportedUUIDException;
use Melia\RecordSet\Filesystem\Exception\UnsupportedNamespaceException;

/**
 * Implementation of FilesystemRecordSet
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class RecordSet extends \PharData implements RecordSetInterface, OffsetConverter {
    /**
     * UUID
     *
     * @var string
     */
    private $uuid;
    /**
     * Namespace
     *
     * @var string
     */
    private $namespace;
    /**
     * Location
     *
     * @var string
     */
    private $location;

    /**
     * Constructor
     *
     * @param string $location            
     * @param string $restoreMetadata            
     */
    public function __construct($location, $restoreMetadata = true) {
        $recordsetExists = file_exists($location);
        parent::__construct($location, \FilesystemIterator::KEY_AS_FILENAME);
        if($recordsetExists && (bool)$restoreMetadata) {
            $this->restoreMetadata();
        }
        $this->setLocation($location);
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }

    /**
     * Set location
     * 
     * @param string $location            
     * @return \Melia\RecordSet\Filesystem\RecordSet
     */
    private function setLocation($location) {
        $this->location = $location;
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\RecordSet\Common\Converter\OffsetConverter::offsetConvert()
     */
    public function offsetConvert($offset) {
        static $offsetConverter;
        if(null === $offsetConverter) {
            $offsetConverter = new NamespaceOffsetConverter();
        }
        $offsetConverter->setNamespace($namespace = $this->getNamespace());
        return $offsetConverter->offsetConvert($offset);
    }

    /**
     * Restore metadata
     * 
     * @throws MissingMetadataException
     * @throws UnsupportedMetadataException
     * @throws MetadataNotFoundException
     * @return \Melia\RecordSet\Filesystem\RecordSet
     */
    public function restoreMetadata() {
        if($this->hasMetadata()) {
            if(is_array($metadata = $this->getMetadata())) {
                if(array_key_exists("uuid", $metadata)) {
                    $this->setUuid($metadata["uuid"]);
                } else {
                    throw new MissingMetadataException("Missing metadata has been detected: uuid");
                }
                if(array_key_exists("namespace", $metadata)) {
                    $this->setNamespace($metadata["namespace"]);
                } else {
                    throw new MissingMetadataException("Missing metadata has been detected: namespace");
                }
            } else {
                throw new UnsupportedMetadataException($metadata);
            }
        } else {
            throw new MetadataNotFoundException("Unable to restore metadata. No metadata has been found.");
        }
        return $this;
    }

    /**
     * Store metadata
     *
     * @return void
     */
    public function storeMetadata() {
        return $this->setMetadata(array(
                "uuid" => $this->getUuid(), 
                "namespace" => $this->getNamespace()));
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \Melia\Uuid\Common\Uuid\UuidAwareInterface::getUuid()
     */
    public function getUuid() {
        return $this->uuid;
    }

    /**
     * Set uuid
     * 
     * @param string $uuid            
     * @throws UnsupportedUUIDException
     * @return \Melia\RecordSet\Filesystem\RecordSet
     */
    public function setUuid($uuid) {
        if(Validator::isSupportedUuid($uuid)) {
            $this->uuid = $uuid;
        } else {
            throw new UnsupportedUUIDException($uuid);
        }
        return $this;
    }

    /**
     * Get namespace
     *
     * @return string
     */
    public function getNamespace() {
        return $this->namespace;
    }

    /**
     * Set namespace
     * 
     * @param string $namespace            
     * @throws UnsupportedNamespaceException
     * @return \Melia\RecordSet\Filesystem\RecordSet
     */
    public function setNamespace($namespace) {
        if(Validator::isSupportedUuid($namespace)) {
            $this->namespace = $namespace;
        } else {
            throw new UnsupportedNamespaceException($namespace);
        }
        return $this;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see PharData::offsetGet()
     */
    public function offsetGet($offset) {
        return parent::offsetGet($offset)->getContent();
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see PharData::getSignature()
     */
    public function getSignature() {
        $this->setSignatureAlgorithm(\Phar::SHA1);
        $signature = parent::getSignature();
        return $signature["hash"];
    }
}