<?php

namespace Melia\RecordSet\Filesystem\Exception;

/**
 * Implementation of MissingMetadataException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class MissingMetadataException extends Exception {
}