<?php

namespace Melia\RecordSet\Filesystem\Exception;

/**
 * Implementation of UnsupportedNamespaceException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UnsupportedNamespaceException extends InvalidArgumentException {
}