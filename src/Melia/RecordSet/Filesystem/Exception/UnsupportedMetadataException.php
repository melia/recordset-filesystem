<?php

namespace Melia\RecordSet\Filesystem\Exception;

/**
 * Implementation of UnsupportedMetadataException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UnsupportedMetadataException extends InvalidArgumentException {
}