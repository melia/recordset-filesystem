<?php

namespace Melia\RecordSet\Filesystem\Exception;

/**
 * Implementation of MetadataNotFoundException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class MetadataNotFoundException extends Exception {
}