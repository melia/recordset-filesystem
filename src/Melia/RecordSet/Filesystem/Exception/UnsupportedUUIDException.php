<?php

namespace Melia\RecordSet\Filesystem\Exception;

/**
 * Implementation of UnsupportedUUIDException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class UnsupportedUUIDException extends InvalidArgumentException {
}