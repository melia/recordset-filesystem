<?php

namespace Melia\RecordSet\Filesystem\Exception;

use Melia\RecordSet\Reference\Exception\InvalidArgumentException as BaseException;

/**
 * Implementation of InvalidArgumentException
 *
 * @author Marvin Elia Hoppe <marvin_elia@web.de>
 *        
 */
class InvalidArgumentException extends BaseException {
}